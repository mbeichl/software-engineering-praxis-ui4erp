# Projekt
Mit dem System sollen Mitarbeiter ihre Arbeitszeit und Tätigkeit erfassen können. Die Zeitenbuchungen sollen einem direkten Projekt zugeordnet werden können und dort einer bestimmten Art von Arbeit. Für die Anwender soll es möglich sein, die "CRUD-Operationen", das heißt Erfassen der Arbeitszeit, bearbeiten einer eigenen, bereits bestehenden Buchung sowie Löschen einer eigenen (fehlerhaften) Buchung, durchführen zu können. Auch Urlaubtage und Krankheitstage sollen damit erfasst werden können, dies ist jedoch im Rahmen des SEP-Projekts eine optionale Anforderung.

Anmerkung: Das Projekt wurde im Rahmen eines Moduls an der TH Rosenheim und mit Zusammenarbeit mit der Codire GmbH gemacht:
<br>
<br>
Teilnehmner: Michael Beichl, Berkan Manap, Sebastian Standl, Simon Sedlmayer, Mathias Nowak
 <br>
**Alle weiteren Informationen sind im Projekt-Wiki einsehbar.**


# Nutzen
- Einheitliche Erfassung von Zeit, Urlaubs- und Krankheitstagen
- Analysewerkzeug für HR-Manager/innen 
- Erstellung von Zeitenabrechnungen für Kundenprojekte
- Leicht zu bedienendes User-Interface 


# Links
- [Teamwiki Startseite](https://inf-git.fh-rosenheim.de/sep-wif-22/ui4erp/-/wikis/home)
- [Development Board](https://inf-git.fh-rosenheim.de/sep-wif-22/ui4erp/-/boards/1221)
- [**Technical** Development Board](https://inf-git.fh-rosenheim.de/sep-wif-22/ui4erp/-/boards/1262?milestone_title=Q2%20-%20Entwicklung&label_name[]=Prio%3A%3A1&label_name[]=Prio%3A%3A2&label_name[]=State%3A%3AImplementing&label_name[]=State%3A%3ATesting&label_name[]=ToDo%3A%3ATechnical)
- [Action Items Board](https://inf-git.fh-rosenheim.de/sep-wif-22/ui4erp/-/boards/1261?milestone_title=None&label_name[]=Action%20Item)
- [Dokumente](https://inf-git.th-rosenheim.de/sep-wif-22/ui4erp/-/tree/main/Dokumente)
- [BigBlueButton](https://conference.th-rosenheim.de/mar-sn7-hin-qum)
- [Homepage Codire GmbH](https://www.codire.de/)
- [Google Drive](https://drive.google.com/drive/folders/1MScKaZkT_E6GlT2KRRI04P3RlYZjf74O)

export const environment = {
  production: true
};

export const envSelectableHours: String[] = ["00","01","02","03","04","05","06","07","08","09","10","11","12",
  "13","14","15","16","17","18","19","20","21","22","23"];
export const envSelectableMinutes : String[] = ["00","15","30","45"];
export const envSelectableBreak: number[] = [0,15,30,45,60];
export const envContentType: String = "application/json";
export const envAPIKey: String = "K5#US4NZkXedP3xRH6cMQ2P$";
export const envURL: String = "https://api.codire.de/graphql";

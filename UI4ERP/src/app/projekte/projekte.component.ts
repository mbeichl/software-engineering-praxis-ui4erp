import { Component, OnInit } from '@angular/core';
import {Project} from "../zeiterfassung/zeiterfassung.service";
import {ProjekteService} from "./projekte.service";
import {username} from "../login/login.component";

@Component({
  selector: 'app-projekte',
  templateUrl: './projekte.component.html',
  styleUrls: ['./projekte.component.css']
})
export class ProjekteComponent implements OnInit {
  codireLogo: String = 'assets/images/codire.png';
  projects: Project[] = [];
  user: String = "";

  constructor(private projekteService:ProjekteService) { }

  async ngOnInit(){
    this.user = username;
    const projectsData =  await this.projekteService.getMyProjects();
    this.projects = projectsData.data.my_projects;
  }
}

import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Token} from "../login/login.component";
import {envContentType, envAPIKey, envURL} from "../../environments/environment.prod";
import {firstValueFrom} from "rxjs";

export interface MyProjects{
  data: MyProjectsData;
}

export interface MyProjectsData{
  my_projects: Project[]
}

export interface Project {
  ID:number;
  NAME:String
}

@Injectable({
  providedIn: 'root'
})

export class ProjekteService {
  projects!: MyProjects;

  constructor(private http:HttpClient) { }

  async getMyProjects():Promise<MyProjects>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  `${envContentType}`,
        Authorization: "Bearer " + `${Token}`,
        'x-api-key': `${envAPIKey}`
      })}
    this.projects = await firstValueFrom(this.http.post(`${envURL}`, {
      "query": "query {my_projects {ID NAME}}"}, httpOptions)) as MyProjects;
    return this.projects;
  }
}

import { Component, OnInit } from '@angular/core';
import {HistorieService, Entry, EntryChecked} from "./historie.service";
import {ActivatedRoute, Router} from "@angular/router";
import {username} from "../login/login.component";

@Component({
  selector: 'app-historie',
  templateUrl: './historie.component.html',
  styleUrls: ['./historie.component.css']
})
export class HistorieComponent implements OnInit {
  codireLogo: String = 'assets/images/codire.png';
  user: String = "";
  entryEdited = false;
  entryDeleted = false;

  /** Aufteilung der Buchungen in invoiced (= abgerechnet beim Kunden) und offene Buchungen */
  invoicedEntries: EntryChecked[] = [];
  openEntries: EntryChecked[] = [];
  entriesCombined: EntryChecked[] = [];

  constructor(private historieService:HistorieService, private route:ActivatedRoute, private router:Router) { }

  async ngOnInit() {
    this.user = username;
    this.entryEdited = false;
    this.entryDeleted = false;
    await this.sortData();
    this.showInfo();
  }

  async deleteClick(timesheetEntryId: number){
    if(confirm(`Wollen Sie die ausgewählte Buchung wirklich löschen?`)) {
      await this.historieService.deleteTimesheetEntry(timesheetEntryId);
      await this.sortData();
      this.entryDeleted = true;
      this.entryEdited = false;
      setTimeout(()=>{this.entryDeleted = false}, 4000);
    }
  }

  editClick(timeSheetEntry:Entry){
    this.router.navigate(['/zeiterfassung', {'data' : JSON.stringify(timeSheetEntry)}]);
  }

  /** Zeigt die Bestätigung, dass eine Buchung bearbeitet wurde, wenn eine Buchungsbearbeitung bestätigt wird */
  showInfo(){
    const routeData = this.route.snapshot.paramMap.get('data');
    if(routeData !== null){
      this.entryEdited = true;
      setTimeout(() => {this.entryEdited = false}, 4000);
    }
  }

  /** comparator Hilfsmethode um Buchungen chronologisch nach Startdatum zu sortieren */
  compare( a : Entry, b :Entry ) {
    if ( a.START.valueOf() < b.START.valueOf()){
      return 1;
    }
    if ( a.START.valueOf() > b.START.valueOf() ){
      return -1;
    }
    return 0;
  }

  /** Hilfsmethode, um abgerechnete Buchungen für das Bearbeiten und Löschen zu blocken
   * Außerdem werden alle Buchungen nach Datum absteigend sortiert und für die Anzeige in einem Array gespeichert
   */
  async sortData(){
    this.entriesCombined = [];
    const invoicedEntriesData =  await this.historieService.getInvoicedEntries();
    const openEntriesData =  await this.historieService.getOpenEntries();

    this.invoicedEntries = (invoicedEntriesData.data.my_timesheet).reverse() as unknown as EntryChecked[];
    this.invoicedEntries.forEach(entry => {entry.invoiced = true;});

    this.openEntries = (openEntriesData.data.my_timesheet).reverse() as unknown as EntryChecked[];
    this.openEntries.forEach(entry => {entry.invoiced = false;});

    this.entriesCombined =  this.entriesCombined.concat(this.invoicedEntries, this.openEntries);
    this.entriesCombined = this.entriesCombined.sort(this.compare);
  }
}

import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Token} from "../login/login.component";
import {envContentType, envAPIKey, envURL} from "../../environments/environment.prod";
import {firstValueFrom} from "rxjs";
import {Task} from "../zeiterfassung/zeiterfassung.service";

export interface MyEntries{
  data: EntriesData;
}

export interface EntriesData{
  my_timesheet: Entry[];
}

export interface Entry{
  ID: number;
  START: Date;
  END: Date;
  DESCRIPTION: String;
  Task: Task;
  BREAK_DURATION: number;
}

export interface EntryChecked{
  ID: number;
  START: Date;
  END: Date;
  DESCRIPTION: String;
  Task: Task;
  BREAK_DURATION: number;
  invoiced: boolean;
}

@Injectable({
  providedIn: 'root'
})

export class HistorieService{
  invoicedEntries!: MyEntries;
  openEntries!: MyEntries;

  constructor(private http:HttpClient) { }

  async getInvoicedEntries():Promise<MyEntries>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  `${envContentType}`,
        Authorization: "Bearer " + `${Token}`,
        'x-api-key': `${envAPIKey}`
      })}
    this.invoicedEntries = await firstValueFrom(this.http.post( `${envURL}`, {
      "query": `query {my_timesheet(invoiced: true){ID START DESCRIPTION END Task{NAME ID} BREAK_DURATION }}`}, httpOptions)) as MyEntries;
    return this.invoicedEntries;
  }

  async getOpenEntries():Promise<MyEntries>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  `${envContentType}`,
        Authorization: "Bearer " + `${Token}`,
        'x-api-key': `${envAPIKey}`
      })}
    this.openEntries = await firstValueFrom(this.http.post(`${envURL}`, {
      "query": `query {my_timesheet(invoiced: false){ID START DESCRIPTION END Task{NAME ID} BREAK_DURATION }}`}, httpOptions)) as MyEntries;
    return this.openEntries;
  }

  async deleteTimesheetEntry(timesheetEntryId : number){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  `${envContentType}`,
        Authorization: "Bearer " + `${Token}`,
        'x-api-key': `${envAPIKey}`
      })}
      await firstValueFrom(this.http.post(`${envURL}`, {
        "query": `mutation {  delete_timesheet_entry (timesheet_entry_id: ${timesheetEntryId})}`}, httpOptions));
    }
}

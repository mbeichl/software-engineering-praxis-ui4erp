import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Token} from "../login/login.component";
import {envContentType, envAPIKey, envURL} from "../../environments/environment.prod";
import {firstValueFrom} from "rxjs";

export interface Project {
  ID: number;
  NAME: String
}

export interface MyTasks{
  data: MyTasksData;
}

export interface MyTasksData{
  my_tasks: Task[]
}

export interface Task{
  ID: number;
  NAME: String;
  PLANNED_HOURS: number;
  Project: Project;
}

@Injectable({
  providedIn: 'root'
})

export class ZeiterfassungService {
  tasks!: MyTasks;

  constructor(private http:HttpClient) { }

  async getMyTasks():Promise<MyTasks>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  `${envContentType}`,
        Authorization: "Bearer " + `${Token}`,
        'x-api-key': `${envAPIKey}`
      })}
    this.tasks = await firstValueFrom(this.http.post(`${envURL}`, {
      "query": `query {my_tasks { ID NAME PLANNED_HOURS Project{ID NAME} }}`}, httpOptions)) as MyTasks;
    return this.tasks;
  }

  async addTimeSheetEntry(taskId:number, startTime:String, endTime:String, breakDuration:number, description:String){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  `${envContentType}`,
        Authorization: "Bearer " + `${Token}`,
        'x-api-key': `${envAPIKey}`
      })}
    await firstValueFrom(this.http.post(`${envURL}`, {
      "query": `mutation {addTimesheetEntry(data: {task_id:${taskId} description:"${description}" break_duration:${breakDuration} start:"${startTime}" end:"${endTime}"}){START END}}`}, httpOptions))
    return;
  }

  async editTimeSheetEntry(taskId:number, startTime:String, endTime:String, breakDuration:number, description:String, timeSheetEntryId:number){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  `${envContentType}`,
        Authorization: "Bearer " + `${Token}`,
        'x-api-key': `${envAPIKey}`
      })}
    await firstValueFrom(this.http.post(`${envURL}`, {
      "query": `mutation {update_timesheet_entry(data: {task_id:${taskId} description:"${description}" break_duration:${breakDuration} start:"${startTime}" end:"${endTime}"}timesheet_entry_id:${timeSheetEntryId}){START END}}`}, httpOptions));
    return;
  }
}


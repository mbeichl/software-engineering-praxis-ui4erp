import {Component, OnInit} from '@angular/core';
import {Task, ZeiterfassungService} from "./zeiterfassung.service";
import {DatePipe, DecimalPipe} from "@angular/common";
import {ActivatedRoute, Router} from "@angular/router";
import {Entry} from "../historie/historie.service";
import {envSelectableBreak, envSelectableHours, envSelectableMinutes} from "../../environments/environment.prod";
import {username} from "../login/login.component";

@Component({
  selector: 'app-zeiterfassung',
  templateUrl: './zeiterfassung.component.html',
  styleUrls: ['./zeiterfassung.component.css']
})
export class ZeiterfassungComponent implements OnInit {
  codireLogo: String = 'assets/images/codire.png';
  tasks: Task[] = [];
  user: String = "";
  starttimeInMinutes! : number;
  endtimeInMinutes! : number;
  selectedTaskID: number = 0;
  /**EndDate = StartDate, da nur über einen Tag gebucht werden kann. Variable EndDate gibt es deshalb nicht*/
  selectedStartDate: Date = new Date(Date.now());
  selectedStartHours = "08";
  selectedEndHours = "08";
  selectedStartMinutes = "00";
  selectedEndMinutes = "15";
  selectableBreakDuration = envSelectableBreak;
  selectedBreakDuration = 0;
  description: String = "";
  selectableMinutes = envSelectableMinutes;
  selectableHours = envSelectableHours;
  tempTime = "00";
  entry!: Entry;
  waiting = false;
  edit = false;
  requestSent = false;
  entryAccepted = false;
  entryError = false;
  entryEditError = false;
  entryBreakError = false;
  errorMessage: String = "";

  constructor(private zeiterfassungsService: ZeiterfassungService, private datePipe: DatePipe, private route:ActivatedRoute, private router:Router,
              private decimalPipe:DecimalPipe) { }

  async ngOnInit() {
    this.user = username;
    const tasksData = await this.zeiterfassungsService.getMyTasks();
    this.tasks = tasksData.data.my_tasks
    this.tasks.forEach(task => {
      task.NAME = task.NAME  + " - " + task.Project.NAME;
    })
    this.selectedStartDate = this.datePipe.transform(this.selectedStartDate, "YYYY-MM-dd") as unknown as Date;
    this.fillForm();
  }

  onKeydown(event: Event, inputName: string) {
    this.tempTime = (event.target as HTMLInputElement).value;
    switch(inputName){
      case 'selectedStartHours':
      case 'selectedEndHours':
        if(envSelectableHours.indexOf(this.tempTime) !== -1){
          /**Eingegebener Wert ist im akzeptierten Wertebereich der Stunden */
          this.onKeydownSave(inputName);
        }
        break;
      case 'selectedStartMinutes':
      case 'selectedEndMinutes':
        if(envSelectableMinutes.indexOf(this.tempTime) !== -1){
          /**Eingegebener Wert ist im akzeptierten Wertebereich der Minuten */
          this.onKeydownSave(inputName);
        }
        break;
      case 'selectedBreakDuration':
        if(envSelectableBreak.indexOf(Number(this.tempTime)) !== -1){
          /**Eingegebener Wert ist im akzeptierten Wertebereich der Pause */
          this.onKeydownSave(inputName);
        }
        break;
    }
  }

  onKeydownSave(inputName: string){
    switch(inputName){
      case 'selectedStartHours':
        this.selectedStartHours = this.tempTime;
        break;
      case 'selectedStartMinutes':
        this.selectedStartMinutes = this.tempTime;
        break;
      case 'selectedEndHours':
        this.selectedEndHours = this.tempTime;
        break;
      case 'selectedEndMinutes':
        this.selectedEndMinutes = this.tempTime;
        break;
      case 'selectedBreakDuration':
        this.selectedBreakDuration = Number(this.tempTime);
        break;
    }

  }

  /**Befüllt die Felder falls die Seite durch bearbeiten aufgerufen wurde*/
  fillForm(){
    const routeData = this.route.snapshot.paramMap.get('data')
    if(routeData !== null){
      this.edit = true;
      this.entry = JSON.parse(routeData);
      this.selectedTaskID = this.entry.Task.ID;
      this.description = this.entry.DESCRIPTION;
      this.selectedBreakDuration = this.entry.BREAK_DURATION;
      this.selectedStartDate = this.datePipe.transform(this.entry.START, "YYYY-MM-dd") as unknown as Date;
      this.selectedStartHours = this.datePipe.transform(this.entry.START, "HH", 'UTC') as unknown as string;
      this.selectedStartMinutes = this.datePipe.transform(this.entry.START, "mm") as unknown as string;
      this.selectedEndHours = this.datePipe.transform(this.entry.END, "HH", 'UTC') as unknown as string;
      this.selectedEndMinutes = this.datePipe.transform(this.entry.END, "mm") as unknown as string;
    }
  }

  async editClick(edit:boolean){
    this.waiting = true;
    this.entryError = false;
    this.entryEditError = false;
    this.entryBreakError = false;
    this.errorMessage = "";
    this.requestSent = true;
    /** Prüfen, ob Pause größer oder gleich Arbeitszeit... */
    this.starttimeInMinutes = Number(this.selectedStartHours) * 60 + Number(this.selectedStartMinutes);
    this.endtimeInMinutes = Number(this.selectedEndHours) * 60 + Number(this.selectedEndMinutes);
    if(this.endtimeInMinutes - this.starttimeInMinutes <= this.selectedBreakDuration){
      /** Wenn ja, Fehlermeldung... */
      this.entryBreakError = true;
    } else {
      /** Sonst für API call transformieren */
      const startDate = this.datePipe.transform(this.selectedStartDate, "YYYY-MM-dd");
      const startHours = this.decimalPipe.transform(this.selectedStartHours, '2.0-0');
      const startMinutes = this.decimalPipe.transform(this.selectedStartMinutes, '2.0-0');
      const endHours = this.decimalPipe.transform(this.selectedEndHours, '2.0-0');
      const endMinutes = this.decimalPipe.transform(this.selectedEndMinutes, '2.0-0');
      if(edit) {
        /** Vorhandene Zeitbuchung bearbeiten */
        await this.zeiterfassungsService.editTimeSheetEntry(this.selectedTaskID, startDate + " " + startHours + ":" + startMinutes, startDate + " " + endHours + ":" + endMinutes, this.selectedBreakDuration, this.description, this.entry.ID)
          .catch((error) => {
            this.errorMessage = "error" + error;
          }
        );
        if (this.errorMessage == "") {
          /** Kein Fehler aus dem API Call */
          await this.router.navigate(['/letzteBuchungen', {'data' : JSON.stringify("1")}]);
        } else {
          /** Fehler aus dem API Call => Fehlermeldung ausgeben */
          this.entryEditError = true;
        }
      }
      else{
        /** Neue Zeitbuchung erstellen */
        await this.zeiterfassungsService.addTimeSheetEntry(this.selectedTaskID, startDate + " " + startHours + ":" + startMinutes, startDate + " " + endHours + ":" + endMinutes, this.selectedBreakDuration, this.description)
          .catch((error) => {
            this.errorMessage = "error" + error;
          }
        );
        if (this.errorMessage == "") {
          /** Kein Fehler aus dem API Call */
          this.selectedTaskID = 0;
          this.selectedStartHours = this.selectedEndHours;
          this.selectedEndHours = "00";
          this.selectedStartMinutes = this.selectedEndMinutes;
          this.selectedEndMinutes = "00";
          this.selectedBreakDuration = 0;
          this.description = "";
          this.entryAccepted = true;
          setTimeout(() => {this.entryAccepted = false}, 4000);
        }
        else {
          /** Fehler aus dem API Call => Fehlermeldung ausgeben */
          this.entryError = true;
        }
      }
    }

    this.waiting = false;
  }
}

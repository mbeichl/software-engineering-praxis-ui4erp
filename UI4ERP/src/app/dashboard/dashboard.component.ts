import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { DashboardService } from './dashboard.service';
import {username} from "../login/login.component";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit  {
  codireLogo: String = 'assets/images/codire.png';
  user: String = "";

  constructor(private dashboardService:DashboardService, private router:Router) {}

  async ngOnInit() {
    this.user = username;
  }

  erfassenclick() {
    this.router.navigateByUrl('zeiterfassung');
  }

  ansehenclick(){
    this.router.navigateByUrl('letzteBuchungen');
  }

  projekteclick(){
    this.router.navigateByUrl('meineProjekte');
  }
}

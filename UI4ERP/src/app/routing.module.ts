import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {DashboardComponent} from "./dashboard/dashboard.component";
import {ZeiterfassungComponent} from "./zeiterfassung/zeiterfassung.component";
import {HistorieComponent} from "./historie/historie.component";
import {ProjekteComponent} from "./projekte/projekte.component";
import {AuthGuardService} from "./authGuardService";

const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent, canActivate:[AuthGuardService] },
  { path: 'login', component: LoginComponent},
  { path: 'zeiterfassung', component: ZeiterfassungComponent, canActivate:[AuthGuardService]},
  { path: 'letzteBuchungen', component: HistorieComponent, canActivate:[AuthGuardService]},
  { path: 'meineProjekte', component: ProjekteComponent, canActivate:[AuthGuardService]},
  { path: '**', redirectTo:'/login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RoutingModule { }

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {NgSelectModule} from "@ng-select/ng-select";
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from "@angular/common/http";
import { RoutingModule } from './routing.module';
import {RouterModule} from "@angular/router";
import { ZeiterfassungComponent } from './zeiterfassung/zeiterfassung.component';

import {FormsModule} from "@angular/forms";
import {DatePipe, DecimalPipe} from "@angular/common";
import { HistorieComponent } from './historie/historie.component';
import { ProjekteComponent } from './projekte/projekte.component';
import {AuthGuardService} from "./authGuardService";


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    ZeiterfassungComponent,
    HistorieComponent,
    ProjekteComponent
  ],
  imports: [
    BrowserModule,
    NgSelectModule,
    FormsModule,
    HttpClientModule,
    RoutingModule,
    RouterModule,
  ],
  providers: [DatePipe, DecimalPipe, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }

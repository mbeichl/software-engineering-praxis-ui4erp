import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {firstValueFrom} from "rxjs";
import {Token} from "./login.component";
import {envContentType, envAPIKey, envURL} from "../../environments/environment.prod";

export interface AuthKey {
  data: Data;
}

export interface Data {
  login: string;
}

export interface UserData{
  data: me;
}

export interface me{
  me: User;
}

export interface User{
  FIRST_NAME: String;
  LAST_NAME: String;
}

@Injectable({
  providedIn: 'root'
})

export class LoginService {
  user!: UserData;
  authKey!: AuthKey;

  constructor(private http:HttpClient) { }

  /** Login-Anfrage an GraphQL mit gegebenem Passwort und Mail-Prefix */
  async login(password:String, emailPrefix:String) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  `${envContentType}`,
        'x-api-key': `${envAPIKey}`
      })}
    this.authKey = await firstValueFrom(
    this.http.post(`${envURL}`, {
      "query": `mutation {login(password: \"${password}\", email: \"${emailPrefix}@codire.de\")}`
    }, httpOptions)) as AuthKey;
    return this.authKey.data.login;
  }

  async getUser(): Promise<UserData> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': `${envContentType}`,
        Authorization: "Bearer " + `${Token}`,
        'x-api-key': `${envAPIKey}`
      })
    }
    this.user = await firstValueFrom(this.http.post(`${envURL}`, {
      "query": "query { me {LAST_NAME FIRST_NAME}}"}, httpOptions)) as UserData;
    return this.user;
  }
}





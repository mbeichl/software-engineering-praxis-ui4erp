import { Component } from '@angular/core';
import {LoginService} from "./login.service";
import {Router} from "@angular/router"

/** Export für andere Components */
export var Token: String = "";
export var username: String = "";
export var isLoggedIn: boolean = false;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent {
  password: String = "";
  emailPrefix: string = "";
  codireLogo: String = 'assets/images/codire.png';
  token: String = "";
  disable: boolean = true;
  fieldTextType: boolean = true;
  requestSent = false;
  user: String = "";
  waiting = false;
  check = true;

  constructor(private loginService: LoginService, private router:Router) { }

  async login(){
    /** variable waiting für Anzeige Ladesymbol im html
    variable check für Anzeige wenn Anmeldung nicht erfolgreich */
    this.check = true;
    this.waiting = true;
    this.requestSent = true;
    this.token = await this.loginService.login(this.password, this.emailPrefix);
    this.requestSent = false;
    /** Erfolgreiche Anmeldung, also variable token befüllt */
    if(this.token !== null){
      isLoggedIn = true;
      /** Nach 30 Minuten wird man ausgeloggt, da dann der Token invalid ist */
      setTimeout(()=>{isLoggedIn = false}, 1800000);
      Token = this.token;
      /** Speichern des eingeloggten Users zum Anzeigen */
      const User =  await this.loginService.getUser();
      this.user = User.data.me.FIRST_NAME + " " + User.data.me.LAST_NAME;
      username = this.user;
      await this.router.navigateByUrl('dashboard');
    }
    /** Falsche Eingaben, Token nicht verfügbar */
    else{
      this.waiting = false;
      this.check = false;
    }
  }

  passwordChanged(event:Event){
    /** Speichern des Passworts bei jeder einzelnen Tastatureingabe */
    this.password = (event.target as HTMLInputElement).value;
  }

  emailChanged(event:Event) {
    /** Speichern des Mail-Prefix bei jeder einzelnen Tastatureingabe */
    this.emailPrefix = (event.target as HTMLInputElement).value;
    const pattern = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]*$/;
    /** Prüfen, ob Eingabe ein valides Mail-Prefix darstellt */
    this.disable = !pattern.test(this.emailPrefix);
  }

  /** Passwort-Input-Typ verändern von Passwort zu Text*/
  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }
}

import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';
import {isLoggedIn} from "./login/login.component";


@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private router:Router ) {
  }

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): boolean {

    /** Wenn die Sitzung abgelaufen ist, wird man wieder auf die Login-Seite geleitet*/
    if (!isLoggedIn)  {
      alert('Ihre Sitzung ist abgelaufen. Bitte loggen Sie sich erneut ein.');
      this.router.navigateByUrl("login");
      return false;
    }
    return true;
  }

}

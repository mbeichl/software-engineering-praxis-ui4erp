# UI4ERP

UI4ERP ist ein Zeiterfassungsfrontend basierend auf Angular.
Um das Projekt starten zu können muss die Long-Term-Support Node.js Version [16.15.1 LTS](https://nodejs.org/dist/v16.15.1/node-v16.15.1-x64.msi) installiert sein.
Um alle Dependencies im Projekt zu installieren muss einmalig der Befehl `npm install` aufgerufen werden. Danach ist das Projekt startklar.
Wir empfehlen Webstorm von JetBrains für die weitere Entwicklung

## Development server

Für den Development Server muss der Befehl `ng serve` ausgeführt werden. Diesen kann man auf `http://localhost:4200/` finden. Alle Änderungen am Code werden automatisch live mit geändert.

## Build

Um das Projekt zu bauen muss der Befehl `ng build` aufgerufen werden. Das gebaute Projekt ist in der Directory `dist/` zu finden


